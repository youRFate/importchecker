use chrono::{Datelike, Utc};
use std::fs;
use std::path::PathBuf;
use structopt::StructOpt;
use walkdir::WalkDir;
extern crate fs_extra;
use fs_extra::copy_items;
use fs_extra::dir::CopyOptions;

/// Search for a pattern in a file and display the lines that contain it.
#[derive(Debug, StructOpt)]
#[structopt(
    name = "importchecker",
    about = "Checks if all images of an SD imported."
)]
struct Cli {
    /// Activate debug mode
    #[structopt(short, long)]
    verbose: bool,

    /// Activate dry run mode
    #[structopt(short, long)]
    dry_run: bool,

    /// SD card base path
    #[structopt(parse(from_os_str))]
    in_path: std::path::PathBuf,

    /// Base Path of the photo archive. Should contain the yearly folders
    #[structopt(parse(from_os_str))]
    collection_path: std::path::PathBuf,
}

fn main() -> Result<(), std::io::Error> {
    let opt = Cli::from_args();
    println!("{:?}", opt);
    // build list of input files
    let mut infiles = Vec::new();
    for entry in WalkDir::new(&opt.in_path)
        .into_iter()
        .filter_map(|e| e.ok())
        .filter(|e| e.path().is_file())
    {
        infiles.push(entry.path().to_owned());
    }
    // now list the destiantion and check
    let mut collected_file_names = Vec::new();
    for entry in WalkDir::new(&opt.collection_path)
        .into_iter()
        .filter_map(|e| e.ok())
        .filter(|e| e.path().is_file())
    {
        collected_file_names.push(entry.path().file_name().unwrap().to_owned())
    }
    let missing_files: Vec<&PathBuf> = infiles
        .iter()
        .filter(|e| {
            !collected_file_names
                .iter()
                .any(|c| c == e.file_name().unwrap())
        })
        .collect();
    if opt.verbose {
        println!("{:?}", missing_files);
    }
    // build output path
    let import_dir: PathBuf = [
        opt.collection_path,
        Utc::now().year().to_string().into(),
        "Unsorted".into(),
        format!("sd_import_{}", Utc::now().format("%Y-%m-%dT%H%M%S")).into(),
    ]
    .iter()
        .collect();
    println!("Importing {} files to directory {:?}.", missing_files.len(), &import_dir);
    if !opt.dry_run {
        fs::create_dir_all(&import_dir)?;
        let options = CopyOptions::new(); //Initialize default values for CopyOptions
        copy_items(&missing_files, &import_dir, &options);
    }
    Ok(())
}
